(defproject stigmergy/webtop "0.1.0-SNAPSHOT"
  :description "A window manager for react.js component written in ClojureScript"
  :url "https://github.com/sonwh98/wim"
  :license {:name "Apache License 2.0"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0-alpha4"]
                 [org.clojure/clojurescript "1.10.238" :scope "provided"]
                 [reagent "0.7.0" :scope "provided"]
                 [garden "1.3.3" :scope "provided"]
                 
                 [com.kaicode/mercury "0.1.2-SNAPSHOT" :scope "provided"]
                 [com.kaicode/material-girl "0.1.0-SNAPSHOT"]
                 [com.kaicode/tily "0.1.6-SNAPSHOT"]
                 [stigmergy/widget "0.1.0-SNAPSHOT"]
                 [stigmergy/dom "0.1.0-SNAPSHOT"]

                 [cljsjs/facebook "v20150729-0"]

                 [com.cognitect/transit-cljs "0.8.243"]

                 [cljfmt "0.5.7"]
                 ]

  :plugins [[lein-cljsbuild "1.1.7"]]

  :min-lein-version "2.7.1"

  :clean-targets ^{:protect false} [:target-path
                                    [:cljsbuild :builds :app :compiler :output-dir]
                                    [:cljsbuild :builds :app :compiler :output-to]]

  :source-paths ["src/cljs" "src"]
  :resource-paths ["resources"]
  :target-path "resources/public/js/generated"

  :cljsbuild  {:builds {:prod {:source-paths ["src/cljs"  "env/prod/cljs"]
                               :compiler {:main stigmergy.webtop.prod
                                          :output-to "resources/public/js/generated/prod/app.js"
                                          :output-dir "resources/public/js/generated/prod"
                                          :source-map "resources/public/js/generated/prod/app.js.map"
                                          :asset-path "/js/generated/prod"
                                          
                                          :optimizations :simple
                                          :verbose true
                                          :pretty-print  true
                                          :parallel-build true}}
                        :dev {:source-paths ["src/cljs" "env/dev/cljs"]
                              :figwheel {:on-jsload "stigmergy.webtop/init!"}
                              :compiler {:main stigmergy.webtop.dev
                                         :asset-path "/js/generated/dev"
                                         :output-to "resources/public/js/generated/dev/app.js"
                                         :output-dir "resources/public/js/generated/dev"
                                         :foreign-libs [{:file "resources/public/js/clojure-parinfer.js"
                                                         :provides ["parinfer.codemirror.mode.clojure.clojure-parinfer"]}]
                                         :source-map true
                                         :optimizations :none
                                         :verbose false
                                         :pretty-print  true
                                         :parallel-build true}}}}
  :figwheel  {:http-server-root "public"
              ;;:nrepl-port 7002
              :nrepl-middleware ["cemerick.piggieback/wrap-cljs-repl"
                                 "cider.nrepl/cider-middleware"
                                 "refactor-nrepl.middleware/wrap-refactor"]
              :css-dirs ["resources/public/css"]}

  :profiles {:dev {:repl-options {:init-ns demo.repl
                                  :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                   :dependencies [[binaryage/devtools "0.9.7"]
                                  [ring/ring-mock "0.3.1"]
                                  [ring/ring-devel "1.6.2"]
                                  [prone "1.1.4"]
                                  [figwheel-sidecar "0.5.14"]
                                  [org.clojure/tools.nrepl "0.2.13"]
                                  [com.cemerick/piggieback "0.2.2"]]
                   :resource-paths ["env/dev/resources"]
                   :source-paths ["env/dev/clj"]
                   :plugins [[lein-figwheel "0.5.14"]
                             [cider/cider-nrepl "0.16.0"]
                             [refactor-nrepl "2.3.1"
                              :exclusions [org.clojure/clojure]]]}})
