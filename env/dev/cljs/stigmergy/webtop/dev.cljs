(ns ^:figwheel-no-load stigmergy.webtop.dev
  (:require [stigmergy.webtop :as webtop]
            [devtools.core :as devtools]
            [figwheel.client :as figwheel :include-macros true]))

(let [protocol window.location.protocol
      ws-url "ws://[[client-hostname]]/figwheel-ws"
      wss-url "wss://[[client-hostname]]/figwheel-ws" ;;need to configure nginx
      websocket-url (if (= "https:" protocol)
                      wss-url
                      ws-url)]
  (figwheel/watch-and-reload
   ;;:websocket-url websocket-url
   :websocket-host :js-client-host 
   ))

(devtools/install!)

(enable-console-print!)

(webtop/init!)
