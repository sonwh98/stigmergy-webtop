(ns demo.repl
  (:require [figwheel-sidecar.repl-api :as ra]))

(defn start []
  (ra/start-figwheel!))

(defn stop []
  (ra/stop-figwheel!))

(defn restart []
  (stop)
  (start))

(defn cljs []
  (ra/cljs-repl))


