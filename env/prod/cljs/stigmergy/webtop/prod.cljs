(ns ^:figwheel-no-load stigmergy.webtop.prod
  (:require [stigmergy.webtop :as webtop]))

;;(set! *print-fn* (fn [& _]))
(enable-console-print!)
(webtop/init!)
