(ns stigmergy.wim
  (:require [clojure.spec.alpha :as s]
            [reagent.core :as r]
            [com.kaicode.mercury :as m]
            [com.kaicode.material_girl :as mdl]
            
            [stigmergy.memory :as mem]
            [stigmergy.dom :as dom]
            [stigmergy.css :as css]))

(def window-element [:wim/id :wim/x :wim/y :wim/z :wim/width :wim/height :wim/ui])

(s/def :wim/window (s/keys :req (conj window-element :window/title)
                           :opt [:window/restore-state  :window/on-close :window/maximized? :window/resizable?]))

(s/def :wim/state (s/keys :req [:wim/windows :wim/next-id]))

(defonce wim-state (mem/malloc [:wim]))
(defonce highest-z-index (atom 1)) ;;notice this is not a reagent atom chosen so that it is not reactive

(defn get-next-z-index []
  (let [z-index @highest-z-index]
    (swap! highest-z-index inc)
    z-index))

(defn add-window [{:keys [wim/x wim/y wim/z wim/width wim/height] :as window-state}]  {:post [(let [valid? (and
                                                                                                            (s/valid? :wim/state @wim-state)
                                                                                                            (s/valid? :wim/window window-state))]
                                                                                                (when-not valid?
                                                                                                  (s/explain :wim/state @wim-state)
                                                                                                  (s/explain :wim/window window-state))
                                                                                                valid?)]}
  (let [[max-width max-height] (dom/get-viewport-dimension)
        add-id (fn [window-state]
                 (let [id (:wim/next-id @wim-state)]
                   (assoc window-state :wim/id id)))
        add-width (fn [window-state]
                    (or (when-not width
                          (let [width (/ max-width 2)]
                            (assoc window-state :wim/width width)))
                        window-state))
        add-height (fn [window-state]
                     (or (when-not height
                           (let [height (* 0.8 max-height)]
                             (assoc window-state :wim/height height)))
                         window-state))
        add-x (fn [window-state]
                (or (when-not x
                      (let [x (max (rand-int (/ max-width 4))
                                   110)]
                        (assoc window-state :wim/x x)))
                    window-state))
        add-y (fn [window-state]
                (or (when-not y
                      (let [y (rand-int (/ max-height 2))]
                        (assoc window-state :wim/y y)))
                    window-state))
        add-z (fn [window-state]
                (or (when-not z
                      (let [z (get-next-z-index)]
                        (assoc window-state :wim/z z)))
                    window-state))
        window-state (-> window-state add-id add-x add-y add-z add-width add-height)]
    (swap! wim-state (fn [state]
                       (let [wim-id (:wim/id window-state)]
                         (-> state
                             (update :wim/next-id inc)
                             (update :wim/windows assoc wim-id window-state)))))))

(defn remove-window [wim-id]
  (swap! wim-state (fn [state]
                     (let [windows (:wim/windows state)
                           windows (dissoc windows wim-id)]
                       (assoc state :wim/windows windows)))))

(defn increment-z [wim-id]
  (swap! wim-state assoc-in [:wim/windows wim-id :wim/z] (get-next-z-index)))

(defn set-window-prop [prop]
  (when-let [wim-id (:wim/id prop)]
    (swap! wim-state update-in [:wim/windows wim-id] merge prop)
    (let [new-width (:wim/width prop)
          new-height (:wim/height prop)
          resized? (if (or (-> new-width nil? not)
                           (-> new-height nil? not))
                     true
                     false)]
      (when resized?
        (let [resize-topic (keyword (str "wim/resize-" wim-id))]
          (m/broadcast [resize-topic new-width new-height]))))))

(def set-window-prop-chan (m/on :stigmergy.wim/set-window-prop (fn [[_ prop]]
                                                                 (set-window-prop prop))))

(defn maximized? [wim-id]
  (get-in @wim-state [:wim/windows wim-id :window/maximized?]))

(defn- resizable? [window-state]
  (let [resizable? (:window/resizable? window-state)]
    (or resizable? (nil? resizable?))))

(defn maximize [wim-id]
  (let [window-state (get-in @wim-state [:wim/windows wim-id])
        x (:wim/x window-state)
        y (:wim/y window-state)
        width (:wim/width window-state)
        height (:wim/height window-state)
        [max-width max-height] (dom/get-viewport-dimension)
        offset-x 10]
    (when (resizable? window-state)
      (set-window-prop {:wim/id wim-id
                        :window/maximized? true
                        :window/restore-state  {:wim/x x
                                                :wim/y y
                                                :wim/width width
                                                :wim/height height}
                        :wim/x offset-x
                        :wim/y 0
                        :wim/z (get-next-z-index)
                        :wim/width (- max-width offset-x)
                        :wim/height max-height}))))

(defn restore [wim-id]
  (let [restore-state (get-in @wim-state [:wim/windows wim-id :window/restore-state ])]
    (set-window-prop (assoc restore-state :wim/id wim-id :window/maximized? false))))

(defn- toggle-max-size [wim-id]
  (if (maximized? wim-id)
    (restore wim-id)
    (maximize wim-id)))

(defonce drag-blank-image (js/Image.))

(defn title-bar [wim-id description]
  (let [delta-xy (atom [0 0])
        last-pointer-xy (atom [0 0])
        window-state (get-in @wim-state [:wim/windows wim-id])]
    (mdl/component (fn [wim-id]
                     (prn "wim-id" wim-id description)
                     (let [set-delta-xy (fn [pointer-x pointer-y]
                                          (let [window-state (get-in @wim-state [:wim/windows wim-id])
                                                current-x (:wim/x window-state)
                                                current-y (:wim/y window-state)
                                                width (:wim/width window-state)
                                                height (:wim/height window-state)
                                                weight-x (/ (- pointer-x current-x) width)
                                                weight-y (/ (- pointer-y current-y) height)
                                                dx (* weight-x width)
                                                dy (* weight-y height)]
                                            (reset! delta-xy [dx dy])))
                           touch-start #(let [touches (-> % .-touches )]
                                          (when-let [one-finger-touch (aget touches 0)]
                                            (increment-z wim-id)
                                            (set-delta-xy (.-clientX one-finger-touch) (.-clientY one-finger-touch))))
                           drag-start (fn [evt]
                                        (let [remove-ghost-image #(.. evt -dataTransfer (setDragImage drag-blank-image 0 0))]
                                          (remove-ghost-image)
                                          (increment-z wim-id)
                                          (set-delta-xy (.. evt -clientX) (.. evt -clientY))))
                           move-window (fn [pointer-x pointer-y]
                                         (let [[max-width max-height] (dom/get-viewport-dimension)
                                               [dx dy] @delta-xy
                                               outofbound? (or (and ;;pointer-x pointer-y are both 0 when mouse draged out of browse window 
                                                                (= 0 pointer-x)
                                                                (= 0 pointer-y))

                                                               ;;outofbound condition with touch
                                                               (>= pointer-x max-width)
                                                               (<= pointer-x 10)
                                                               (>= pointer-y max-height)
                                                               (<= pointer-y 10))
                                               [pointer-x pointer-y] (if outofbound?
                                                                       @last-pointer-xy
                                                                       [pointer-x  pointer-y])
                                               x (- pointer-x dx)
                                               y (- pointer-y dy)]
                                           (when-not (and (= 0 pointer-x)
                                                          (= 0 pointer-y))
                                             (reset! last-pointer-xy [pointer-x pointer-y]))
                                           (if (maximized? wim-id)
                                             (restore wim-id)
                                             (set-window-prop {:wim/id wim-id :wim/x x :wim/y y}))))
                           drag-move-window #(move-window (.. % -clientX) (.. % -clientY))
                           touch-move-window #(let [touches (-> % .-touches )]
                                                (when (dom/iOS?)
                                                  (.. % preventDefault))
                                                (when-let [one-finger-touch (aget touches 0)]
                                                  (move-window (.-clientX one-finger-touch) (.-clientY one-finger-touch))))
                           button-style {:float :right
                                         :margin-top 5}
                           title-bar-color "#cce6ff"
                           title-bar-height 40]
                       [:div {:style {:background-color title-bar-color
                                      :height title-bar-height}
                              :draggable true
                              :on-drag-start drag-start
                              :on-drag drag-move-window
                              :on-touch-start touch-start
                              :on-touch-move touch-move-window
                              :on-double-click #(toggle-max-size wim-id)}
                        [:button.mdl-button.mdl-js-button.mdl-button--icon {:style button-style
                                                                            :on-click (fn [evt]
                                                                                        (let [window-state (get-in @wim-state [:wim/windows wim-id])
                                                                                              on-close (:window/on-close window-state)]
                                                                                          (on-close)
                                                                                          (.. evt stopPropagation)
                                                                                          (remove-window wim-id)))}
                         [:i.material-icons {:style {:font-size 24}} "close"]]

                        (when (resizable? window-state)
                          [:button.mdl-button.mdl-js-button.mdl-button--icon {:style button-style
                                                                              :on-click (fn [evt]
                                                                                          (.. evt stopPropagation)
                                                                                          (toggle-max-size wim-id))}
                           [:i.material-icons {:style {:font-size 24}} "crop_square"]])
                        
                        description])))))

(defn resize-border [wim-id]
  (let [resize (fn [pointer-x pointer-y]
                 (let [window-state (get-in @wim-state [:wim/windows wim-id])
                       x (:wim/x window-state)
                       y ( :wim/y window-state)
                       z (get-next-z-index)
                       width (- pointer-x x)
                       height (- pointer-y y)]
                   (set-window-prop {:wim/id wim-id :wim/z z :wim/width width :wim/height height})))
        mouse-resize #(resize (.. % -clientX) (.. % -clientY))
        touch-resize (fn [evt]
                       (let [touches (-> evt .-touches )]
                         (when-let [one-finger-touch (aget touches 0)]
                           (resize (.-clientX one-finger-touch) (.-clientY one-finger-touch)))))
        corner-css {:background-color :transparent
                    :position :absolute
                    :right 0
                    :bottom 0
                    :width 20
                    :height 20
                    :border-right "3px solid black"
                    :border-bottom "3px solid black"
                    :cursor :nwse-resize}]
    [:div {:style corner-css
           :draggable true
           :on-drag-start #(.. % -dataTransfer (setDragImage drag-blank-image 0 0))
           :on-drag mouse-resize
           :on-drag-end mouse-resize
           :on-touch-move touch-resize}]))

(defn window [window-state]
  (let [width (:wim/width window-state)
        height (:wim/height window-state)
        title-bar-height 40
        wim-id (:wim/id window-state)
        z (:wim/z window-state)
        focus? (= (inc z) @highest-z-index)
        user-select (if focus?
                      :auto
                      :none)]
    [:div {:class :wim-window
           :data-wim-id wim-id
           :style {:position    :absolute
                   :left        (:wim/x window-state)
                   :top         (:wim/y window-state)
                   :z-index     (:wim/z window-state)
                   :width       width
                   :height      height
                   :border      "1px solid "
                   :-webkit-user-select user-select}
           :on-click (fn [evt]
                       ;;(.. (js/window.getSelection) empty) ;;prevent drag and drop froms selecting text
                       (increment-z wim-id))}
     [title-bar wim-id (:window/title window-state)]
     [:div {:style {:height   (- height title-bar-height)
                    :overflow :auto}}
      (:wim/ui window-state)]
     (when (resizable? window-state)
       [resize-border wim-id])
     ]))

(defn main []
  (swap! wim-state assoc
         :wim/windows {}
         :wim/next-id 0)
  (fn []
    [:div {:id    "wm-main"
           :style (merge {:width    "100%"
                          :height   "100%"
                          :overflow :hidden}
                         css/prevent-copy)}
     (for [[id window-state] (-> @wim-state :wim/windows)]
       ^{:key id} [window window-state])]))

