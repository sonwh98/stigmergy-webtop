(ns stigmergy.panel
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [stigmergy.memory :as mem]
            [stigmergy.wim :as wim]
            [stigmergy.loader :as loader]
            [stigmergy.dom :as dom]
            [stigmergy.css :as css]

            [com.kaicode.material_girl :as mdl]
            [reagent.core :as r]))

(defonce panel-thickness 50)

(defn show-panel [panel-state]
  (swap! panel-state assoc :visible? true :wim/z (wim/get-next-z-index)))

(defn hide-panel  [panel-state]
  (swap! panel-state assoc :visible? false))

(defn set-launched? [panel-state app-id t-or-f]
  (swap! panel-state assoc-in [:apps app-id :app/launched?] t-or-f))

(defn launch [panel-state panel-element]
  (let [element-id (:panel-element/id panel-element)
        element-name (:panel-element/name panel-element)
        src (:app/src panel-element)
        app-module (:app/module panel-element)
        wim-ui (:wim/ui panel-element)]

    (when-not (:app/launched? panel-element)
      (cond
        (-> app-module nil? not) (go (let [main (<! (loader/load-symbol {:base-url "/js/dev" :module-name app-module} 'main))]
                                       (set-launched? panel-state element-id true)
                                       (wim/add-window {:window/title element-name :wim/ui [main]
                                                        :window/on-close #(set-launched? panel-state element-id false)})))
        (-> wim-ui nil? not) (do
                               (set-launched? panel-state element-id true)
                               (wim/add-window {:window/title element-name :wim/ui wim-ui
                                                :wim/width (:wim/width panel-element) :wim/height (:wim/height panel-element)
                                                :window/resizable?  (:window/resizable? panel-element)
                                                :window/on-close #(set-launched? panel-state element-id false)}))))))

(defn panel-icon [panel-state panel-element]
  (let [element-name (:panel-element/name panel-element)
        icon-name (:material/icon panel-element)
        mouseover? (r/atom false)]
    (fn [panel-state panel-element]
      (let [font-size (or (:thickness @panel-state) panel-thickness)
            prop {:id element-name
                  :class "material-icons"
                  :style (if @mouseover?
                           {:font-size font-size
                            :opacity   0.5}
                           {:font-size font-size})
                  :on-mouse-over #(reset! mouseover? true)
                  :on-mouse-out #(reset! mouseover? false)}
            prop (cond
                   (-> panel-element :wim/ui nil? not) (assoc prop :on-click (fn []
                                                                               (prn "click" panel-element)
                                                                               (launch panel-state panel-element)))
                   (-> panel-element :panel-element/on-click nil? not) (assoc prop :on-click (:panel-element/on-click panel-element))
                   :else prop)]
        [:div
         [:i prop icon-name]
         [:div {:class "mdl-tooltip mdl-tooltip--large mdl-tooltip--right"
                :data-mdl-for element-name}
          element-name]]))))



(def main (mdl/component (fn [panel-state & content]
                           (let [panel-color "#cce6ff"
                                 event-listeners (if (dom/iOS?)
                                                   {:on-click #(swap! panel-state (fn [panel-state]
                                                                                    (-> panel-state
                                                                                        (update-in [:visible?] not)
                                                                                        (assoc :wim/z (wim/get-next-z-index)))))}
                                                   {:on-mouse-over #(show-panel panel-state)
                                                    :on-mouse-leave #(hide-panel panel-state)})
                                 position (:panel/position @panel-state)
                                 gripper-size 10
                                 gripper-style (condp = position
                                                 :left {:background-color :transparent
                                                        :width gripper-size
                                                        :height "100%"}
                                                 :top {:background-color :transparent
                                                       :width "100%"
                                                       :left 0
                                                       :top 0
                                                       :position :absolute
                                                       :height gripper-size})
                                 thickness (or (:panel/thickness @panel-state) panel-thickness)
                                 panel-style (condp = position
                                               :left {:background-color panel-color
                                                      :width            thickness
                                                      :height           "100%"
                                                      :top (:wim/y @panel-state)
                                                      :position         :absolute
                                                      :z-index          (:wim/z @panel-state)
                                                      :display          :flex
                                                      :flex-direction   :column
                                                      :border-width 1}
                                               :top {:background-color panel-color
                                                     :width            "100%"
                                                     :height           thickness
                                                     :position         :absolute
                                                     :left             0
                                                     :top              0
                                                     :z-index          (:wim/z @panel-state)
                                                     :display          :flex
                                                     :flex-direction   :row-reverse
                                                     :border-style :solid
                                                     :border-width 1})]
                             [:div (merge {:style gripper-style}
                                          event-listeners)
                              (when (:visible? @panel-state)
                                [:div {:style (merge panel-style
                                                     css/prevent-copy)}
                                 (concat (for [[id panel-element] (:panel/elements @panel-state) ]
                                           ^{:key (str "app-" id)} [panel-icon panel-state panel-element])
                                         
                                         (for [[i c] (map-indexed (fn [index c] [index c]) content)]
                                           (with-meta  c {:key (str "content-" i)})))])]))))
