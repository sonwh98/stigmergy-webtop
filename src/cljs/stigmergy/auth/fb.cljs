(ns stigmergy.auth.fb
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [stigmergy.loader :as loader]
            [com.kaicode.tily :as tily]
            [com.kaicode.mercury :as m]
            [cljs.core.async :as async :refer [<! >! chan]]
            [clojure.walk :as w]
            [clojure.string :as s]))

(defn- getLoginStatus [{:keys [on-connected-fn on-not-connected-fn]}]
  (.. js/FB (getLoginStatus (fn [response]
                              (let [status (aget response "status")]
                                (if (= status "connected")
                                  (on-connected-fn response)
                                  (on-not-connected-fn response)))))))

(defn- on-connected [response]
  (let [fields ["id" "first_name" "last_name" "gender" "email" "is_verified" "verified"]]
    (.. js/FB (api "/me" (clj->js {:fields (s/join ","  fields)})
                   (fn [result]
                     (let [user-profile (-> result js->clj
                                            (select-keys fields)
                                            (w/keywordize-keys))
                           id (:id user-profile)
                           user-profile (assoc user-profile :profile-img-url (tily/format "https://graph.facebook.com/%s/picture?type=small" id))]
                       (m/broadcast [:pam/user-profile user-profile])))))))

(defn- init [{:keys [fb-id] :as params}]
  (aset js/window "fbAsyncInit" (fn []
                                  (prn "asyncInit")
                                  (.. js/FB (init (clj->js {:appId fb-id
                                                            :cookie true
                                                            :xfbml true
                                                            :version "v2.12"}))))))

(defn login []
  (.. js/FB (login (fn [response]
                     (prn "login")
                     (let [auth-response (aget response "authResponse")]
                       (when auth-response
                         (on-connected response)
                         )))
                   (clj->js {:scope "public_profile, email"}))))

(defn ^:export logout []
  (.. js/FB (logout)))

(defn load-script []
  (go (init {:fb-id "923680817780859"})
      (<! (loader/load {:url "https://connect.facebook.net/en_US/sdk.js"
                        :method :script-tag}))
      (<! (async/timeout 500))
      (getLoginStatus {:on-connected-fn on-connected
                       :on-not-connected-fn (fn [response])})))


(load-script)
