(ns stigmergy.cdr
  (:require [stigmergy.memory :as mem]
            [reagent.core :as r]
            [garden.core :refer [css]]
            [goog.style]
            [cljfmt.core :as f]
            [cljs.js :as cljs]
            [cljs.pprint :as pp]))

(goog.style/installStyles (css [".CodeMirror" {:border "1px solid red"
                                               :height "100%"
                                               }]))

(defonce cljs-state (cljs/empty-state))
(defonce cdr-state (mem/malloc [:cdr]))
(reset! cdr-state {:editors []
                   :active-editor-id nil})

(defn active-editor? [id]
  (= (:active-editor-id @cdr-state)
     id))

(defn editor [editor-state]
  (r/create-class {:component-did-mount (fn [this-component]
                                          (when-not (:editor/code-mirror @editor-state)
                                            (let [this-element (r/dom-node this-component)
                                                  cm (js/CodeMirror. this-element #js{:lineNumbers true
                                                                                      :mode :clojure
                                                                                      :smartIndent true})]
                                              (.. js/parinferCodeMirror (init cm))
                                              (swap! editor-state assoc :editor/code-mirror cm))))
                   :reagent-render (fn [editor-state]
                                     (let [id (:editor/id @editor-state)]
                                       [:div {:id id
                                              ;;:class "w3-container w3-display-container"
                                              :style {:position :absolute
                                                      :width "100%"
                                                      :height "100%"
                                                      :visibility (if (active-editor? id)
                                                                    :visible
                                                                    :hidden)}}]))}))


(defn find-editor [id]
  (->> @cdr-state :editors
       (filter (fn [editor-state]
                 (= (:editor/id editor-state) id)))
       first))

(defn format-code []
  (let [active-editor-id (:active-editor-id @cdr-state)
        active-editor (->> @cdr-state :editors
                           (filter (fn [editor-state]
                                     (= (:editor/id editor-state) active-editor-id)))
                           first)
        code-mirror (:editor/code-mirror active-editor) 
        code-str (.. code-mirror (getSelection))
        formatted-code (f/reformat-string code-str)]
    (.. code-mirror (replaceSelection formatted-code))))

(defn toolbar []
  (let [toolbar-css {:margin 5}]
    [:div {:style {:display :flex
                   :justify-content :center
                   :background-color :grey}}
     [:button {:style toolbar-css
               :on-click (fn []
                           (swap! cdr-state (fn [cdr-state]
                                              (let [id (rand-int 100)
                                                    editor-state {:editor/id id
                                                                  :editor/file-name "no-name"}
                                                    cdr-state (update-in cdr-state [:editors] conj editor-state)]
                                                (if (> (-> cdr-state :editors count) 1)
                                                  cdr-state
                                                  (assoc cdr-state :active-editor-id id))))))} "New"]
     [:button {:style toolbar-css
               :on-click format-code} "Format"]
     [:button {:style toolbar-css
               :on-click #(let [active-editor-id (:active-editor-id @cdr-state)
                                editor (find-editor active-editor-id)
                                file-name (:editor/file-name editor)
                                cm (:editor/code-mirror editor)
                                src (.. cm getSelection)]
                            (prn "src=" src)
                            (cljs/eval-str cljs-state src file-name {:eval cljs/js-eval} identity))}
      "Eval"]
     [:button {:style toolbar-css} "Compile"]]))

(defn tab-editors []
  [:div
   [:div {:class "w3-bar "}
    (doall (for [index (-> @cdr-state :editors count range)
                 :let [editor-state (r/cursor cdr-state [:editors index])
                       id (:editor/id @editor-state)
                       file-name (:editor/file-name @editor-state)]]
             ^{:key index} [:span {:class "w3-bar-item w3-button"
                                   :on-click #(swap! cdr-state assoc :active-editor-id id)}
                            file-name
                            [:i {:class "material-icons"
                                 :on-click (fn [evt]
                                             (.. evt stopPropagation)
                                             (swap! cdr-state (fn [cdr-state]
                                                                (let [editors (vec (remove (fn [e] (= id (:editor/id e)))
                                                                                           (:editors cdr-state)))
                                                                      active-id (-> editors first :editor/id)]
                                                                  (assoc cdr-state
                                                                         :editors (vec editors)
                                                                         :active-editor-id active-id)))))} "close"]]))]

   (doall (for [index (-> @cdr-state :editors count range)
                :let [editor-state (r/cursor cdr-state [:editors index])
                      id (:editor/id @editor-state)
                      file-name (:editor/file-name @editor-state)]]
            ^{:key index} [editor editor-state]))]
  
  )

(defn main []
  [:div {:style {:position :relative
                 :background-color :white
                 :width "100%"
                 :height "100%"
                 :overflow :hidden}}
   [toolbar]
   [tab-editors]])

