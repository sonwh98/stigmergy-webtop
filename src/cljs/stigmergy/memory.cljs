(ns stigmergy.memory
  (:require [reagent.core :as r ]))

(defonce state (r/atom {}))

(defn malloc
  ([path]
   (r/cursor state path))
  ([path init-state]
   (let [mem (malloc path)]
     (reset! mem init-state)
     mem)))

(defn free [path]
  (swap! state dissoc path))

(defn get [path]
  (malloc path))
