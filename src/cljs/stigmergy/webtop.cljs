(ns stigmergy.webtop
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as r]
            [stigmergy.memory :as mem]
            [stigmergy.wim :as wim]            
            [stigmergy.webtop.app-launcher :as app-launcher]
            [stigmergy.webtop.top-panel :as top-panel]
            [stigmergy.dom :as dom]
            [stigmergy.pam :as pam]
            [garden.core :refer [css]]
            [goog.style]))

(defn init! []
  (let [app-div (js/document.getElementById  "app")
        web-top (fn [pam-state]
                  (let [authenticated? (reaction (some-> @pam-state :user-profile nil? not))]
                    (fn [pam-state]
                      (into [:div {:style {:width "100%"
                                           :height "100%"}}]
                            (if @authenticated?
                              [[top-panel/main]
                               [app-launcher/main]
                               [wim/main]]
                              [[pam/login-view]])))))
        webtop-color "#996666"]
    
    (when (dom/iOS?)
      ;;https://stackoverflow.com/questions/7768269/ipad-safari-disable-scrolling-and-bounce-effect
      (aset app-div "ontouchmove" #(.. % preventDefault)))
    
    (goog.style/installStyles (css [[:body {:height "100%"
                                            :overflow :hidden}]
                                    ["#app"  {:background-image (str "url('/img/zencircle.png')")
                                              :background-color webtop-color
                                              :background-repeat :no-repeat
                                              :background-size :contain
                                              :background-position :center
                                              :height "100%"
                                              :overflow :hidden}]
                                    [".literally" {:width "100%"
                                                   :height "100%"}]
                                    [".re-console-container" {:flex 1
                                                              :text-align :left
                                                              :background-color "#FAFAFA"
                                                              :overflow-y :scroll
                                                              :height "100%"
                                                              :padding 10
                                                              :border-top-left-radius 10
                                                              :border-top-right-radius 10
                                                              :-webkit-transform "translateZ(0)"
                                                              :-webkit-mask-image "-webkit-radial-gradient(circle, white 100%, black 100%)"}]
                                    [".re-console" [".CodeMirror" {:font-size 15
                                                                   :margin-bottom 20}
                                                    ".CodeMirror-code" {:background-color "#FFF" }]]
                                    [".re-console-mode-line" {:font-family "Menlo,Monaco,Consolas,'Courier New',monospace"
                                                              :background-color "#414141"
                                                              :text-align :center
                                                              :color :white
                                                              :padding 5
                                                              :margin 0
                                                              :border-bottom-left-radius 10
                                                              :border-bottom-right-radius 10
                                                              :-webkit-transform "translateZ (0)"
                                                              :-webkit-mask-image "-webkit-radial-gradient (circle, white 100%, black 100%)"
                                                              :white-space :pre-wrap
                                                              :word-wrap :break-word}]
                                    
                                    ]))
    (r/render [web-top pam/state] app-div)))
