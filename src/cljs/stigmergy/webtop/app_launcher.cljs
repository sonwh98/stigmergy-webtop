(ns stigmergy.webtop.app-launcher
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [stigmergy.memory :as mem]
            [stigmergy.dom :as dom]
            [stigmergy.wim :as wim]
            [stigmergy.panel :as panel]
            [stigmergy.loader :as loader]

            [reagent.core :as r]
            [clojure.core.async :refer [<! >!]]
            [com.kaicode.material_girl :as mdl]
            [com.kaicode.mercury :as m]

            [demo.contacts :as contacts]
            [demo.tiger :as tiger]
            [demo.chess :as chess]
            [demo.browser :as browser]
            [demo.paint :as paint]
            [demo.irc :as irc]
            ;;[demo.shell :as shell]
            [stigmergy.cdr :as cdr]))

(def full-screen-action {:panel-element/name "Toggle Full Screen"
                         :material/icon "fullscreen"
                         :panel-element/on-click (let [fullscreen? (atom false)]
                                                   (fn [evt]
                                                     (if @fullscreen?
                                                       (.. js/document webkitExitFullscreen)
                                                       (.. js/document.body webkitRequestFullscreen))
                                                     (swap! fullscreen? not)))})

(def tile-window-action {:panel-element/name "Tile Windows"
                         :material/icon "view_stream"
                         :panel-element/on-click (fn [evt]
                                                   (let [windows (mem/get [:wim :wim/windows])
                                                         [width height] (dom/get-viewport-dimension)
                                                         width (* 0.99 width)
                                                         height (* 0.99 height)
                                                         window-height (/ height (count @windows))]
                                                     
                                                     (doseq [[index  [id window]] (map-indexed vector @windows)
                                                             :let [y (* index window-height)]]
                                                       (wim/set-window-prop {:wim/id id
                                                                             :wim/height window-height
                                                                             :wim/width width
                                                                             :wim/x 10
                                                                             :wim/y y}))))})

(defn search-dock [panel-state]
  (let [visible? (r/atom false)]
    (m/on :search-dock/toggle-visible #(swap! visible? not))
    (fn []
      [:div
       (when @visible?
         [(mdl/component (fn []
                           (let [[width height] (dom/get-viewport-dimension)]
                             [:div {:style {:position         :absolute
                                            :background-color "#e2edff"
                                            :opacity          0.90
                                            :left             50
                                            :width            (max 400 (* 0.5 width))
                                            :height           (* 0.5 height)
                                            :top              0}}
                              [:div {:class "mdl-textfield mdl-js-textfield"}
                               [:input {:class "mdl-textfield__input", :type "text", :id "search"}]
                               [:label {:class "mdl-textfield__label", :for "search"} "Search..."]]])))])])))

(defn main []
  (let [panel-elements [{:panel-element/name "Search"
                         :material/icon "search"
                         :panel-element/on-click #(m/broadcast [:search-dock/toggle-visible :toggle])}
                        {:panel-element/name "Contacts"
                         :material/icon "phone"
                         :wim/ui [contacts/main]
                         :app/launched? false}
                        {:panel-element/name "Literally Canvas"
                         :material/icon "format_paint"
                         :wim/ui [paint/main]
                         :app/launched? false}
                        {:panel-element/name "Tiger SVG"
                         :material/icon "image"
                         :wim/ui [tiger/main]
                         :wim/width 300
                         :wim/height 300
                         :app/launched? false}
                        {:panel-element/name "IRC"
                         :material/icon "chat"
                         :wim/ui [irc/main]
                         :app/launched? false
                         }
                        ;; {:panel-element/name "CLJS Shell"
                        ;;  :material/icon "desktop_mac"
                        ;;  :wim/ui [shell/main]
                        ;;  :app/launched? false}
                        {:panel-element/name "CDR IDE"
                         :material/icon "build"
                         :wim/ui [cdr/main]
                         :app/launched? false}
                        {:panel-element/name "Web Browser"
                         :material/icon "cloud"
                         :wim/ui [browser/main]
                         :app/launched? false}
                        {:panel-element/name "Li Chess TV"
                         :material/icon "videogame_asset"
                         :wim/ui [chess/main]
                         :window/resizable? false
                         :wim/width 240
                         :wim/height 320
                         :app/launched? false}]
        panel-elements (if (dom/iOS?) ;;iOS does not have fullscreen API
                         (concat panel-elements tile-window-action)
                         (concat panel-elements [full-screen-action tile-window-action]))
        
        id-element-pairs (map-indexed (fn [index app]
                                        [index (assoc app :panel-element/id index)])
                                      panel-elements)
        panel-elements-map (into {} id-element-pairs)
        left-panel-state (mem/malloc [:left-panel] {:visible? true
                                                    :panel/position :left
                                                    :wim/z (wim/get-next-z-index)
                                                    :wim/y 0
                                                    :panel/elements panel-elements-map})]
    [:div {:style {:top 0
                   :left 0
                   :width 50
                   :height "100%"}}
     [panel/main left-panel-state
      [search-dock left-panel-state]]]))


(comment
  (go (let [data (<! (loader/GET "/launcher-config.edn"))]

        ))
  )
