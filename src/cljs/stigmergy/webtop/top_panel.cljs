(ns stigmergy.webtop.top-panel
  (:require [stigmergy.memory :as mem]
            [stigmergy.pam :as pam]
            [stigmergy.wim :as wim]
            [com.kaicode.tily :as tily]
            [stigmergy.panel :as panel]))

(def thickness 40)
(defn profile-button []
  (if-let [user-profile (pam/get-user-profile)]
    [:button.mdl-button.mdl-js-button.mdl-button--icon.mdl-js-ripple-effect
     {:style {:background-position :center
              :height thickness
              :width thickness
              :border :solid
              :border-width 1
              :background-image  (tily/format "url(%s)" (:profile-img-url user-profile))
              :background-size :cover}
      :on-click pam/logout}]
    [:i {:class "material-icons" :style {:font-size 50}} "account_circle"]))

(defn main []
  (let [top-panel-state (mem/malloc [:top-panel] {:visible? true
                                                  :panel/position :top
                                                  :panel/thickness thickness
                                                  :wim/z (wim/get-next-z-index)})]
    [panel/main top-panel-state
     [profile-button]]))
