(ns stigmergy.pam ;;Pluggable Authentication Module
  (:require [stigmergy.memory :as mem]
            [stigmergy.auth.fb :as fb]
            [com.kaicode.mercury :as m]))

(def state (mem/malloc [:pam] {:user-profile nil}))

(m/on :pam/user-profile (fn [[_ user-profile]]
                          (swap! state assoc :user-profile user-profile)                          ))

(defn not-implemented []
  (js/alert "not implemented"))

(defn anonymous-login []
  (swap! state assoc :user-profile {:profile-img-url "http://graphics.wsj.com/how-i-learned-to-love-writing-with-emojis/img/emoji/smiley.png"}))

(defn login-button [props txt]
  [:button (merge props
                  {:class "mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"})
   [:img {:style {:height 20
                  :width 20
                  :margin-right 5}
          :src (str "/img/" (:icon props))}] txt])

(defn login-view []
  [:div {:class "mdl-card mdl-shadow--6dp"
         :style {:margin-left  :auto
                 :margin-right :auto
                 :margin-top   "10px"}}
   [:div {:style {:background-color "#cce6ff"}
          :class "mdl-card__title"}
    [:h2 {:class "mdl-card__title-text"} (str "Welcome to Webtop" )]]
   [:div {:class "mdl-card__actions mdl-card--border"}
    [login-button {:on-click fb/login :icon "Facebook.png"} "FB Login"]
    [login-button {:on-click not-implemented :icon "Linkedin.png"} "Linked-in Login"]
    [login-button {:on-click not-implemented :icon "Google+.png"} "G+ Login"]
    [login-button {:on-click not-implemented :icon "Twitter.png"} "Twitter Login"]
    [login-button {:on-click anonymous-login :icon "dummy_auth.jpg"} "Anonymous login"]]])

(defn get-user-profile []
  (:user-profile @state))

(defn logout []
  (swap! state dissoc :user-profile))
