(ns stigmergy.loader
  (:require [clojure.string :as s]
            [goog.net.XhrIo :as xhr]
            [clojure.core.async :refer [chan close! <! >!]])
  (:require-macros [cljs.core.async.macros :refer [go]]))


;;map to track what modules have been loaded.
;;the key is url and the val is either module-name or the url itself {url url|module-name}
(defonce loaded-modules (atom {}))

(defn GET [url]
  (let [ch (chan 1)]
    (xhr/send url
              (fn [event]
                (let [res (-> event .-target .getResponseText)]
                  (go (>! ch res)
                      (close! ch)))))
    ch))

(defn loaded?
  "predicate to determines whether module is loaded already"
  [module-name-or-url]
  (or (contains? (set (keys @loaded-modules)) module-name-or-url)
      (contains? (set (vals @loaded-modules)) module-name-or-url)))

(defn clear-loaded-modules []
  (reset! loaded-modules {}))

(defn ->file-path [module-name]
  (-> module-name
      str
      (s/replace #"\.|-" {\. "/" \- \_})))

(defn load
  "module-name and url are mutually exclusive. only provide one or the other.
  method is either :script-tag or :xhr, defaults to :xhr. Use :script-tag to avoid CORS issues. "
  [{:keys [module-name url base-url method] :or {base-url "js" method :xhr}}]
  (let [c (chan)]
    (go (let [url (or url
                      (let [module-path (-> module-name ->file-path)
                            module-url (str base-url "/" module-path ".js")]
                        module-url))
              module-name (or module-name url)]
          (if (loaded? url)
            (do
              (prn "already loaded " url)
              (>! c true))
            (case method
              :xhr        (let [script-txt (<! (GET url))]
                            (js/globalEval script-txt)
                            (swap! loaded-modules assoc url module-name)
                            (>! c true))
              :script-tag (let [script-element (js/document.createElement "script")
                                onload (fn [evt]
                                         (swap! loaded-modules assoc url module-name)
                                         (go (>! c true)))]
                            (aset script-element "src" url)
                            (aset script-element "onload" onload)
                            (.. js/document.body (appendChild script-element)))))))
    c))

(defn as-map [module-name]
  (ns-interns* (symbol module-name)))

(defn load-symbol
  "return the value of a-symbol in the given module-name. For example: (load-symbol 'blu.auth.fb 'logout)"
  [{:keys [base-url module-name] :as params} a-symbol]
  (go (<! (load params))
      (let [module-as-map (as-map module-name)
            a-var (module-as-map (symbol a-symbol))]
        (when a-var
          @a-var))))

;;https://groups.google.com/forum/#!topic/clojurescript/QqZz560erLM%5B1-25%5D
;;using js/globalEval is indirect call to eval and thus forces it to evaluate in global scope
(js/eval "globalEval = eval") 

(comment
  (go (<! (load {:url "/js/dev/demo/svg.js"}))
      (prn "done")
      )

  (clear-loaded-modules)
  )
