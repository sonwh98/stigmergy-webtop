(ns demo.irc
  (:require [reagent.core :as r]))


(defn main []
  (let [loaded? (r/atom false)]
    (fn []
      [:div {:style {:width "100%"
                     :height "100%"
                     :background-color :white}}
       [:iframe {:src "https://kiwiirc.com/client/irc.kiwiirc.com/#lambdakids"
                 :style {:width "100%"
                         :height "100%"}
                 :on-load #(swap! loaded? not)}]
       
       (when-not @loaded?
         [:div {:style {:position :absolute
                        :left 0
                        :top 100
                        :width "100%"
                        :height "100%"
                        }}
          [:img {:src "/img/loading.gif"}]
          [:h2"Please wait. This takes a while"]])

       ])))
