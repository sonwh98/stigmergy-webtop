(ns demo.paint
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as r]
            [stigmergy.loader :as loader]
            [cljs.core.async :as async :refer [<!]]))

(def main (r/create-class {:component-did-mount (fn [this-component]

                                                  (let [lc (r/dom-node this-component)
                                                        react-chan (loader/load {:url "/js/paint/react-0.14.3.js"})
                                                        lc-chan (loader/load {:url "/js/paint/literallycanvas.js"})
                                                        merged-chan (async/merge [react-chan lc-chan])]
                                                    (go
                                                      (<! merged-chan)
                                                      (js/LC.init lc #js{:imageURLPrefix "/img/paint/"
                                                                         :toolbarPosition "bottom"
                                                                         ;;:defaultStrokeWidth 2
                                                                         ;;:strokeWidths [1, 2, 3, 5, 30]
                                                                         }))))
                           :reagent-render (fn []
                                             [:div {:style {:width "99%" :height "99%" }}
                                              [:div {:id "lc"
                                                     }]])}))
