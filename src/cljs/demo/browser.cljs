(ns demo.browser
  (:require [reagent.core :as r]))

(defn main
  "events in iframe does not bubble up. this causes bugs"
  []
  (let [url (r/atom "https://www.clojure.org")
        url-text-field (r/atom @url)]
    (fn []
      [:div {:style {:background-color :white
                     :width "100%"
                     :height "100%"
                     :overflow :hidden}}
       [:div {:style {:display :flex}}
        [:label {:id "url"
                 :style {:flex-grow 0}} "URL"]
        [:input {:style {:flex-grow 1
                         :margin-left 5
                         :margin-right 5}
                 :value @url-text-field
                 :on-change #(reset! url-text-field (-> % .-target .-value))
                 :on-key-press #(let [char-code (.-charCode %)]
                                  (when (= char-code 13)
                                    (if (re-find #"https*://" @url-text-field)
                                      (reset! url @url-text-field)
                                      (let [http-url (str "http://" @url-text-field)]
                                        (reset! url-text-field http-url)
                                        (reset! url http-url)))))}]]
       [:iframe {:src @url
                 :frameBorder 0
                 :width "100%"
                 :height "100%"}]])))
