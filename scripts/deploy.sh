docker exec -it webtop rm -rf /stigmergy-webtop
docker cp tmp webtop:/stigmergy-webtop
docker stop webtop
docker start webtop
docker exec -td webtop sh -c "/stigmergy-webtop/start.sh"
