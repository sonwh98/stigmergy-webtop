# README #

Stigmergy Webtop is a desktop like environment for Single Page Application written in ClojureScript. Any React.js component can be managed as an "app" window, though only tested with reagent components.

One of the long term goal is the ability to create app without a backend. As a developer, you will only need to worry about working and quering local state. There will be a mechanism for synchronizing your state to a 
global decentralized database like a blockchain without the costs. The backend will be a global or private distributed decentralized peer-to-peer tuple space enhanced with datalog

The default launcher is modeled after ubuntu unity.

Demo at http://webtop.stigmergy.systems/

### How do I get set up? ###

* lein repl
* (start)
* http://localhost:3349

